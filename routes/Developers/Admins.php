<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use GuzzleHttp\Middleware;

Route::controller(AdminController::class)
        ->prefix('admin')
        ->middleware('admin')
        ->group(function(){
            Route::post('product/accept','accept');
            Route::post('product/reject','reject');

        });

    
  