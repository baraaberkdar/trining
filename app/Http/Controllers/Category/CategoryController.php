<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Trait\{
    responseTrait,
    uplodeImages,
};
use App\Http\Requests\CategoryRequest\{
    StoreCategoryRequest,
    UpdateCategoryRequest
};
use  App\Models\Categorie;
use DB;

class CategoryController extends Controller
{
    use responseTrait, uplodeImages;

    public function index()
    {
        // local scope GetCategoryAndProduct
        $cats = Categorie::MainAndSubCategory()->GetCategoryAndProduct()
            ->get();
        return $this->returnData('data', $cats);
    }

    public function store(StoreCategoryRequest $request)
    {
        try {
            DB::beginTransaction();
            $cat = Categorie::create([
                "name"              => $request->name,
                "descreption"       => $request->descreption,
                "category_id"       => $request->category_id
            ]);
            $name = $this->saveImages([$request->file], "Categorie")[0];
            $cat->image()->create(["image_name" => $name]);
            if ($cat) {
                DB::commit();
                return $this->returnSucess('200', "the data inserted sucsssfly");
            }
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->returnError("201", $ex->getMessage());
        }
    }
    public function show($id)
    {

        $catregory = Categorie::MainAndSubCategory()
            ->GetCategoryAndProduct()->find($id);
        if ($catregory) {
            return $this->returnData('data', $catregory);
        } else {
            return $this->returnError("201", "the Category not found");
        }
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        DB::beginTransaction();
        $cat = Categorie::find($id);
        $sub = Categorie::find($request->sub_categore_id);
        if ($cat) {
            if ($sub) {
                $cat->hasCategory()->delete();
                $sub->belongsToGategory()->associate($cat)->save();
            }
            $updated      = $cat->update($request->except(['file', 'sub_categore_id']));
            $update_image = $this->Update_image($request, 'file', 'Categorie', $cat); //Categorie is disk in fileSysetm.php
            if ($updated &&  $update_image) {
                DB::commit();
                return $this->returnSucess("200", "the data updated sucssfly");
            } else {
                return $this->returnError("201", 'error in updated process');
            }
        } else {
            DB::rollback();
            return $this->returnError('201', "the data not found");
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $cat = Categorie::find($id);
            if ($cat) {
                $name = $cat->image()->pluck('image_name');
                // $this->deleteImages($name, 'Categorie');
                $cat->image()->delete();
                $deleted = $cat->delete();
                if ($deleted) {
                    DB::commit();
                    return $this->returnSucess('200', "the data deleted sucssfly");
                } else {
                    return $this->returnError("201", 'error in deleted process');
                }
            } else {
                return $this->returnError('201', "the data not found");
            }
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->returnError('201', $ex->getMessage());
        }
    }
}
