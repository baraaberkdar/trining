<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Trait\responseTrait;
use App\Trait\uplodeImages;
use App\Trait\UplodeTrait;
use App\Http\Requests\UserRequest\StoreUserRequest;
use App\Http\Requests\UserRequest\UpdateUserRequest;
use App\Http\Requests\UserRequest\UpdatePasswordRequest;
use Carbon\Carbon;
use DB;

class UserController extends Controller
{
    use responseTrait, uplodeImages, UplodeTrait;

    public function index()
    {
        $users = User::with('image')->get();
        return $this->returnData("data", $users);
    }

    public function store(StoreUserRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $user = User::create([
                "name"   => $request->name,
                "email"  => $request->email,
                'password' => $request->password,
            ]);
            $names = $this->saveImages([$request->file], "User")[0];
            $user->image()->create(['image_name' => $names]);
            return $this->returnData("data", $user, 'the data inserted sucssfly');
        });
    }

    public function show($id)
    {

        $user = User::find($id);
        if ($user) {
            return $this->returnData('data', $user);
        } else {
            return $this->returnError("201", "the user not found");
        }
    }

    public function update(UpdateUserRequest $request, $id)
    {
        return DB::transaction(function () use ($request,$id) {
            $user = User::find($id);
            $user->update($request->all());

            if ($request->hasFile('file')) {
                $old_image = $user->image()->pluck('image_name');
                $names = $this->saveImages([$request->file], "User")[0];
                $user->image()->update(['image_name' => $names]);
            }
            return $this->returnSucess('200', "the data updated sucssfly");

        });


        // try {
        //     if ($user) {
        //         DB::beginTransaction();
        //         if ($request->hasFile('file')) {
        //             $old_image = $user->image()->pluck('image_name');
        //             $names = $this->saveImages([$request->file], "User")[0];
        //             $user->image()->update(['image_name' => $names]);
        //         }
        //         if ($user) {
        //             DB::commit();
        //             return $this->returnSucess('200', "the data updated sucssfly");
        //         } else {
        //             return $this->returnError('201', "error in  update process ");
        //         }
        //     } else {
        //         return $this->returnError('201', "the user not found");
        //     }
        // } catch (\Exception $ex) {
        //     DB::rollback();
        //     return $this->returnError('201', $ex->getMessage());
        // }
    }
    public function destroy(Request $request, $id)
    {

            return DB::transaction(function() use ($request,$id){
                $user = User::find($id);
                $user->delete();
                $old_image = $user->image()->pluck('image_name');
                $this->deleteImages($old_image, 'User');
                $user->image()->delete();
                return $this->returnSucess('200', 'the data deleted sucssfly');
            });
        // try {
        //     DB::beginTransaction();
        //     if ($user) {
        //         $deleted = 
        //         if ($deleted) {
        //             Db::commit();
        //             return $this->returnSucess('200', 'the data deleted sucssfly');
        //         } else {
        //             return $this->returnError("201", 'error in delete process');
        //         }
        //     } else {
        //         return $this->returnError('201', 'the user not found');
        //     }
        // } catch (\Exception $ex) {
        //     DB::rollback();
        //     return $this->returnError('201', $ex->getMessage());
        // }
    }

    public function change_passowrd(UpdatePasswordRequest $request)
    {
        $user = User::find($request->id);
        $updated = $user->forceFill([
            "password" => $request->password
        ])->save();
        if ($updated) {
            return $this->returnSucess('200', 'the password updated sucssfly');
        } else {
            return $this->returnError('201', "error in  update process ");
        }
    }
}
