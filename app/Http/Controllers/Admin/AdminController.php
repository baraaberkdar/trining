<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AcceptAndRejectRequest;
use App\Models\Product;
use App\Models\User;

use App\Trait\responseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redis;

class AdminController extends Controller
{
    use responseTrait;

    public function accept(AcceptAndRejectRequest $request)
    {
        $product    = Product::find($request->product_id);
        $admin_name = User::find($request->admin_id)->name;
        if ($product) {
            $product->update(['status' => 2]);
            // notiifacation 
            $user = $product->hasUser;
            $user->notfiyEmail($product, $admin_name, "Accepted By Admin");
            return $this->returnSucess(200, "The product Accepted ");
        } else {
            $this->returnError(201, "The product Not found");
        }
    }

    public function reject(AcceptAndRejectRequest $request)
    {
        $product = Product::find($request->product_id);
        if ($product) {
            $product->update(['status' => 0]);
            $admin_name = User::find($request->admin_id)->name;
            // notiifacation 
            $user = $product->hasUser;
            $user->notfiyEmail($product, $admin_name, "Rejected By Admin");
            return $this->returnSucess(200, "The product Reject  ");
        } else {
            $this->returnError(201, "The product Not found");
        }
    }
}
