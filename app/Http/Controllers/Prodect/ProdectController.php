<?php

namespace App\Http\Controllers\Prodect;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Http\Requests\ProductRequest\{

    StoreProdectRequest,
    UpdateProductRequest
};
use App\Trait\{
    responseTrait,
    uplodeImages
};
use App\Scopes\ProductScope;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;


class ProdectController extends Controller
{
    use responseTrait, uplodeImages;


    public function index()
    {
        // Glpal Scope in Product 
        $products = Product::all();
        return $this->returnData('data', $products);
    }

    public function store(StoreProdectRequest $request)
    {

        return DB::transaction(function () use ($request) {
            $product = Product::create($request->except('file'));
            $names   = $this->saveImages($request->file, 'Product');
            $product->createManyImage($names);
            // notifacation 
            $user = User::whereRelation('role', 'name', "Admin")->get();
            Notification::send($user, new \App\Notifications\DataBase\AddProducts($product));
            return $this->returnSucess('200', "The product added successfully ,please wait to accept it by admin");
        });
    }



    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return $this->returnData('200', $product);
        } else {
            return $this->returnError('201', "the product not found");
        }
    }
    public function update(Request $request, $id)
    {
        return DB::transaction(function () use ($request, $id) {

            $request1 = $request->except(['file']);
            $product = Product::withoutGlobalScope(ProductScope::class)->find($id);
            $updated       = $product->update($request1);
            if ($request->hasFile('file')) {
                $old_names  = $product->image()->pluck('image_name');
                $names      = $this->saveImages($request->file, 'Product');
                $product->image()->delete();
                $product->createManyImage($names);
                $this->deleteImages($old_names, "Product");
            }
            return $this->returnSucess('200', 'the data updated sucssfly');
        });
    }

    public function destroy(Request $request, $id)
    {


        return DB::transaction(function () use ($id) {
            $product = Product::withoutGlobalScope(ProductScope::class)->find($id);
            $images = $product->image()->pluck('image_name');
            $deleted = $product->delete();
            $product->image()->delete();
            $this->deleteImages($images, 'Product');
            return $this->returnSucess('200', 'the data deleted sucssfly');
        });
    }
}
