<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {   
          
            $admin_id = $request->admin_id;
            $user = User::find($admin_id);
            if($user->role->name=="Admin"){
                return $next($request);
            }else{
                return response()->json(["Status"=>"false","msg"=>"Not Allowed"],401);
            }

    }
}
