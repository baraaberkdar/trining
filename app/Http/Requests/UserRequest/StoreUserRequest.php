<?php

namespace App\Http\Requests\UserRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Trait\responseTrait;

class StoreUserRequest extends FormRequest
{
    use responseTrait;
 
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'        => 'required|string|max:100',
            'email'       => 'required|string|email|max:100|unique:user',
            'password'    => 'required|string|min:8|numeric',
            'file'           => 'required|image|mimes:gif,png,jpg|dimensions:max_width=3840,max_height=2160|max:2700',
        ];
    }

    public function messages(){
        return [
            "required"       =>"the input is required",
            "string"         =>"the input must be string",
            "email"          =>"the email must br vaild email",
            "name.max"       =>"the maxmum charcters is 100",
            "email.unique"   =>"the email must be unique",
            "password.min"   =>"the password must be grether than 8",
            'file.required'              =>"the image input is required",
            'file.image'                 =>"the image must by type image",
            'file.mims'                  =>"the image must be gif or png or jpg type only"   
        ];
    }


    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        throw new HttpResponseException($this->returnError('201',$errors->first()));
    }
    

}
