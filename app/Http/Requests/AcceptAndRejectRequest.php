<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Trait\responseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class AcceptAndRejectRequest extends FormRequest
{
    use responseTrait;
    public function authorize(): bool
    {
        return true;
    }

   

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
         "product_id"  => "required|exists:products,id" ,
         "admin_id"    => "required|exists:user,id"
        ];
    }
    public function messages(){
        return [
            "product_id.required"              =>"the product_id filed  is required",
            "admin_id.required"             =>"the admin_id filed  is required",
            "product_id.exists"                   =>"the product not exsist ",
            "admin_id.admin_id"                =>"the admin not exsist "   
        ];

    }

    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        throw new HttpResponseException($this->returnError('201',$errors->first()));
    }
}
