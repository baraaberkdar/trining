<?php

namespace App\Http\Requests\ProductRequest;

use Illuminate\Foundation\Http\FormRequest;

use App\Trait\responseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class StoreProdectRequest extends FormRequest
{
    use responseTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            "name"          =>'max:100|string|required|unique:products',
            "description"   =>'required|string',
            'category_id'   =>'required|exists:categories,id',
            "user_id"       =>'required|exists:user,id',
            'file'          =>'array|min:2|required',
            'file.*'        =>'image|mimes:gif,png,jpg|dimensions:max_width=3840,max_height=2160|max:2700',
            'price'         =>"required"
        ];
    }

    public function messages(){
        return [
            "name.required"              =>"the input name is required",
            "description.required"       =>"the input descreption is required",
            "name.max"                   =>"the name filed must be max 100 char",
            "name.unique"                =>"the name filed must be unique",
            "string"                     =>"the input must be string",
            'category_id.required'       =>"the category id is required",
            'category_id.exists'         =>"the category not found",
            'file.*.required'              =>"the image input is required",
            'file.*.image'                 =>"the image must by type image",
            'file.*.mims'                  =>"the image must be gif or png or jpg type only"   
        ];

    }

    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        throw new HttpResponseException($this->returnError('201',$errors->first()));
    }

}
