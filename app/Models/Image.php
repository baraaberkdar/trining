<?php

namespace App\Models;
use Illuminate\Database\Eloquent\
{
    Factories\HasFactory,
    Relations\MorphOne,
    Model             ,
};

class Image extends Model
{
    use HasFactory;
    protected $table='images';
    
    protected $guarded=[];

    protected $hidden=['imageable_type','created_at','updated_at','imageable_id'];
   
    public function imageable()
    {
        return $this->morphTo();
    }

    // public function getImageNameAttribute($val){
    //     $model=explode("\\",$this->imageable_type);
    //     $foldername=end($model);
    //     return asset("/$foldername")."/".$val;
    // }
}
