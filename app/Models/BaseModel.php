<?php

namespace App\Models;
use Illuminate\Database\Eloquent\
{
    Factories\HasFactory,
    Model             
};

class BaseModel extends Model
{
    use HasFactory;


    protected $hidden = ['created_at','updated_at'];
    protected $appends= ['created_from','images'];


    public function getCreatedFromAttribute(){
        return $this->created_at->diffForHumans();
    }


    public function getImagesAttribute()
    {
        return $this->image()
        ->get(['image_name','imageable_type'])
        ->map(function($image){
            $model=explode("\\",$image->imageable_type);
            $foldername=end($model);
            return asset("/$foldername")."/".$image->image_name;
        });

    }


}
