<?php

namespace App\Models;

use Illuminate\Database\Eloquent\
{
    Factories\HasFactory,
    Relations\MorphOne,
    Model             ,
};
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Builder;

class Categorie extends BaseModel 
{ 
    use HasFactory;
    protected $table = 'categories';
    public $timestamps = true;
    protected $guarded=[];

    protected $hidden=['updated_at','created_at','categore_id'];

    // protected $appends =['created_from','images'];



    public function hasProdect()
    {
        return $this->hasMany('App\Models\Product', 'category_id');
    }
    public function hasCategory(){

        return $this->hasMany('App\Models\Categorie','category_id')->with(['hasCategory','hasProdect']);
    }
    public function belongsToGategory(){
        return $this->belongsTo('App\Models\Categorie','category_id');
    }



    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    
    public function scopeMainAndSubCategory(Builder $query){
        $query->whereNull('category_id') //  category_id Null => MainCategory
             ->with(['hasCategory'])->get();
    }


    public function scopeGetCategoryAndProduct (Builder $query){
     
        $query->with(['hasProdect'=>function($q){
            $q->with('hasUser')->whereHas('hasUser',function($q){
                $q->where('name','like','%a%');
            })->get();
        }]);
    }
    // public function getCreatedFromAttribute(){
    //     return $this->created_at->diffForHumans();
    // }

    
    // public function getImagesAttribute()
    // {
    //     return $this->image()
    //     ->get(['image_name','imageable_type'])
    //     ->map(function($image){
    //         $model=explode("\\",$image->imageable_type);
    //         $foldername=end($model);
    //         return asset("/$foldername")."/".$image->image_name;
    //     });

    // }



}