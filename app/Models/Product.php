<?php

namespace App\Models;

use Illuminate\Database\Eloquent\
{
    Factories\HasFactory,
    Relations\MorphOne,
    Model             ,
};
use App\Scopes\ProductScope;
use Database\Factories\UserFactory;

class Product extends BaseModel 
{
    use HasFactory;
    protected $table = 'products';
    public $timestamps = true;
    protected $guarded=[];
    protected $hidden=['updated_at','created_at','category_id','user_id'];
    // protected $appends =['created_from','images'];

    // Register Product Modle in Global Scope (ربط المودل بال سكوب)
    protected static function booted()
    {
        static::addGlobalScope(new ProductScope);
    }

  

    public function hasCategory()
    {
        return $this->belongsTo('App\Models\Categorie', 'category_id');
    }
    public function hasUser(){
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function getStatusAttribute($val){
        return $val ==1 ?"active":"not active";
    }

    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    // public function getCreatedFromAttribute(){
    //     return $this->created_at->diffForHumans();
    // }
 

    public function createManyImage($images){
        foreach($images as $image){

            $this->image()->create(["image_name"=>$image]);

        }


    }

    

        
    // public function getImagesAttribute()
    // {
    //     return $this->image()
    //     ->get(['image_name','imageable_type'])
    //     ->map(function($image){
    //         $model=explode("\\",$image->imageable_type);
    //         $foldername=end($model);
    //         return asset("/$foldername")."/".$image->image_name;
    //     });

    // }

}