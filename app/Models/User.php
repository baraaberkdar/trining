<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Database\Eloquent\
{
    Factories\HasFactory,
    Relations\MorphOne,
    Model             ,
};
use Database\Factories\UserFactory;

class User extends BaseModel 
{
    use HasFactory,Notifiable;

    protected $table = 'user';
    public $timestamps = true;
    public $fillable=['name','email','created_at','password'];
    protected $hidden=['updated_at','created_at','password'];

    protected $casts=[
        'password' =>"hashed"
    ];
    protected $appends =['created_from','images'];
    
    // Get the User image
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }


    public function role(){
        return $this->belongsTo('App\Models\Role','role_id');
    }


    public function notfiyEmail($product,$admin_name="",$status){
    
        Notification::send($this, new \App\Notifications\Email\SendEmail($product, $admin_name,$status));
    }



    // public function getCreatedFromAttribute($val){
    //     return $this->created_at->diffForHumans();
    // }
    
    // public function getImagesAttribute()
    // {
    //     return $this->image()
    //     ->get(['image_name','imageable_type'])
    //     ->map(function($image){
    //     $model=explode("\\",$image->imageable_type);
    //     $foldername=end($model);
    //     return asset("/$foldername")."/".$image->image_name;
    //     });

    // }

}