<?php

namespace App\Jobs;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class DeleteProductAndSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

 
    public function __construct()
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        $products=Product::where('status',0)->get();
        foreach ($products as $product){
            $user=$product->hasuser;
            $user->notfiyEmail($product,"Deleted From System","");
           $product->delete();
             }     

    }
}
