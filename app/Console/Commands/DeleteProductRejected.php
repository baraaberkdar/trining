<?php

namespace App\Console\Commands;

use App\Jobs\DeleteProductAndSendEmail;
use Illuminate\Console\Command;
class DeleteProductRejected extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-product-rejected';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete All product Rejectd By Admin';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DeleteProductAndSendEmail::dispatch();
    }
}
