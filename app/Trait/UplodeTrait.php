<?php

namespace App\Trait;
use App\Models\Image;
use Illuminate\Http\Request;
use Storage;
trait UplodeTrait

{

    public function UplodeFIleAndSave(Request $request, $inputname  , $disk, $model){

        if( $request->hasFile( $inputname ) ) {
            // make file name an store on the server
            $photo = $request->file($inputname);
            $name = rand()."_".$model->id;
            $filename = $name. '.' . $photo->getClientOriginalExtension();
            // // insert Image  into dataBasae
            $image=$model->image()->create([
                "image_name"      =>$filename
            ]);
            if($image){
                // uploded on the server 
            return $photo->storeAs("/",$filename, $disk);
            }
        }

        return null;

    }


    public function UplodeMultiFile(Request $request, $inputname  , $disk,$imageable_id,$imageable_type,$model){
        if($request->hasFile($inputname) && count($request->file($inputname))>=2 ){
            $data=[];
            $images=$request->file($inputname);

        // make file name an store on the server
         foreach($images as $image){
            $name = rand()."_".$imageable_id;
            $filename = $name. '.' . $image->getClientOriginalExtension();
             $data[] =[
                 "image_name"      =>$filename,
                 "imageable_type"  =>$imageable_type,
                 "imageable_id"    =>$imageable_id         
                 
             ];
                // uploded on the server 
            $image->storeAs("/", $filename, $disk);
            }
            // // insert Image into dataBasae

           $created=$model->image()->insert($data);
         
            return $created;
       }else{
        return false;
       }
    }

    public function DeleteMultiFile($disk,$model){
        $count=$model->image->count();
        if($count >= 2){
            foreach ($model->image as $image){

                $filename=explode("/",$image->image_name); // لان خاصية اسم الصورة تم عمل لها دالة ضمن الموديل 
                $oldfilename=end($filename);  
                // delete From server
                Storage::disk($disk)->delete("/".$oldfilename);
            }
            }
    
        // deleted from dataBase
        $deleted= $model->image()->delete();
        return $deleted;
      
    }
    public function DeleteFile($disk,$model){
        $image=$model->image;
        $filename=explode("/",$image->image_name); // لان خاصية اسم الصورة تم عمل لها دالة ضمن الموديل 
        $oldfilename=end($filename);  
        // delete From server
        Storage::disk($disk)->delete("/".$oldfilename);
        $deleted= $model->image()->delete();
        return $deleted;

    }

    public function Update_image(Request $request,$inputname,$disk,$model){
        if($request->hasFile($inputname)){
    
                $image=$request->file($inputname);
                $filename=explode("/",$model->image->image_name); // used to delete file from server 
                $oldfilename=end($filename);   
                   
                $newfilename=rand()."_".$model->id.".".$image->getClientOriginalExtension();//new image name 
                $uploded= Storage::disk($disk)->delete("/".$oldfilename);
                $image->storeAs("/", $newfilename, $disk);
                $model->image->update(["image_name"=>$newfilename]);
    }
    
    return true;
}

public function UpdatedMultiImage(Request $request,$inputname,$disk,$model){
    if($request->hasFile($inputname)){
    $images=$request->file($inputname);
    if(count($images)>=2){

        foreach($model->image as $image){
            $filename=explode("/",$image->image_name); // used to delete file from server 
            $oldfilename=end($filename);   
            $uploded= Storage::disk($disk)->delete("/".$oldfilename);
    
        }
        $model->image()->delete();
        foreach($images as $image){
    
            $newfilename=rand()."_".$model->id.".".$image->getClientOriginalExtension();//new image name 
            $image->storeAs("/", $newfilename, $disk);
            $model->image()->updateOrCreate(['image_name'=>$newfilename]);
        }
        
        
    }
}

return true;

    }
}
