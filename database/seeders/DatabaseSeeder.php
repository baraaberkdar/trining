<?php

namespace Database\Seeders;
use Database\Seeders\{
    UserSeed,
    CatSeed ,
    ProductSeed,
    RoleSeeder
};




// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CatSeed::class,
            ProductSeed::class,
            UserSeed::class,
            RoleSeeder::class    

        ]);
    }
}
