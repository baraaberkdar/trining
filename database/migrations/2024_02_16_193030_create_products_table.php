<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->id();
			$table->string('name', 100);
			$table->text('description');
			$table->tinyInteger('status')->default('1');
			$table->float('price');
			$table->timestamps();
			$table
			->foreignId('category_id')
			->constrained('categories')
			->cascadeOnDelete()	
		    ->cascadeOnUpdate();

			$table->foreignId('user_id')
				  ->constrained('user')
				  ->cascadeOnDelete()
				  ->cascadeOnUpdate();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}