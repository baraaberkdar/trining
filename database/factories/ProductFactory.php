<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Categorie;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
        "name"          =>$this->faker->randomElement(['A10',"A20","A30","A12","A50"]),
         "description"  =>"description",
         'cat_id'       =>Categorie::all()->random()->id  
        ];
    }
}
